drop table if exists host;
drop table if exists object;
drop table if exists reviews;
drop table if exists allocation;
drop table if exists pricing;
drop table if exists object_location;

create table host (
	id_host int NOT NULL PRIMARY KEY,
	host_url varchar(1000) NULL,
	host_name varchar(500) NOT NULL,
	host_since date null,
	host_location varchar(1000) null,
	host_response_time varchar(100) null,
	host_response_rate int null,
	host_acceptance_rate varchar(100) null,
	host_is_superhost bool null,
	host_neighbourhood text null,
	host_listings_count int null,
	host_total_listings_count int null,
	host_verifications text null,
	host_identity_verified bool null
);

create table object (
	id_object int NOT NULL PRIMARY KEY,
	id_host int NOT NULL references host(id_host),
	listing_url varchar(1000) NULL,
	id_scrape long NULL,
	last_scraped DATE,
	name varchar(1000) NOT NULL,
	space text NULL,
	description text NULL,
	daily_price float null,
	beds int null,
	review_scores_rating int null
);

create table object_location (
	id_object int NOT NULL PRIMARY KEY references object(id_object),
	city varchar(200) NOT NULL,
	state varchar(200) NOT NULL,
	zipcode int NULL,
	market varchar(200) NOT NULL,
	smart_location varchar(200) NOT NULL,
	country_code varchar(5) NOT NULL,
	country varchar(200) NOT NULL,
	latitude float NULL,
	longitude float NULL,
	is_location_exact bool NULL,
	street varchar(200) NOT NULL
);

create table pricing (
	id_object int NOT NULL PRIMARY KEY references object(id_object),
	weekly_price float null,
	monthly_price float null,
	security_deposit float null,
	cleaning_fee float null
);

create table reviews (
	id_object int NOT NULL PRIMARY KEY references object(id_object),
	review_scores_accuracy int null,
	review_scores_cleanliness int null,
	review_scores_checkin int null,
	review_scores_communication int null,
	review_scores_location int null,
	review_scores_value int null,
	reviews_per_month float null,
	number_of_reviews int null,
	first_review date null,
	last_review date null
);

create table allocation (
	id_object int NOT NULL PRIMARY KEY references object(id_object),
	guests_included int null,
	extra_people varchar(20) null,
	minimum_nights int null,
	maximum_nights int null,
	has_availability bool null,
	availability_30 int null,
	availability_60 int null,
	availability_90 int null,
	availability_365 int null,
	property_type varchar(100) null,
	room_type varchar(100) null,
	accommodates int null,
	bathrooms float null,
	bedrooms int null,
	bed_type varchar(100) null,
	amenities text null,
	square_feet int null
);