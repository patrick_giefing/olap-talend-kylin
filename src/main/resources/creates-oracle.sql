drop table reviews;
drop table allocation;
drop table pricing;
drop table object_location;
drop table object;
drop table host;

create table host (
	id_host number(10, 0) NOT NULL PRIMARY KEY,
	host_url varchar2(1000) NULL,
	host_name varchar2(500) NOT NULL,
	host_since date null,
	host_location varchar2(1000) null,
	host_response_time varchar2(100) null,
	host_response_rate number(10, 0) null,
	host_acceptance_rate varchar2(100) null,
	host_is_superhost NUMBER(1,0) null,
	host_neighbourhood clob null,
	host_listings_count number(10, 0) null,
	host_total_listings_count number(10, 0) null,
	host_verifications clob null,
	host_identity_verified NUMBER(1,0) null
);

create table object (
	id_object number(10, 0) NOT NULL PRIMARY KEY,
	id_host number(10, 0) NOT NULL references host(id_host),
	listing_url varchar2(1000) NULL,
	id_scrape number(18, 0) NULL,
	last_scraped DATE,
	name varchar2(1000) NOT NULL,
	space clob NULL,
	description clob NULL,
	daily_price number(10, 2) null,
	beds number(10, 0) null,
	review_scores_rating number(10, 0) null
);

create table object_location (
	id_object number(10, 0) NOT NULL PRIMARY KEY references object(id_object),
	city varchar2(500) NOT NULL,
	state varchar2(500) NOT NULL,
	zipcode number(10, 0) NULL,
	market varchar2(500) NOT NULL,
	smart_location varchar2(500) NOT NULL,
	country_code varchar2(10) NOT NULL,
	country varchar2(200) NOT NULL,
	latitude number(4, 32) NULL,
	longitude number(4, 32) NULL,
	is_location_exact NUMBER(1,0) NULL,
	street varchar2(500) NOT NULL
);

create table pricing (
	id_object number(10, 0) NOT NULL PRIMARY KEY references object(id_object),
	weekly_price number(10, 2) null,
	monthly_price number(10, 2) null,
	security_deposit number(10, 2) null,
	cleaning_fee number(10, 2) null
);

create table reviews (
	id_object number(10, 0) NOT NULL PRIMARY KEY references object(id_object),
	review_scores_accuracy number(10, 0) null,
	review_scores_cleanliness number(10, 0) null,
	review_scores_checkin number(10, 0) null,
	review_scores_communication number(10, 0) null,
	review_scores_location number(10, 0) null,
	review_scores_value number(10, 0) null,
	reviews_per_month number(10, 2) null,
	number_of_reviews number(10, 0) null,
	first_review date null,
	last_review date null
);

create table allocation (
	id_object number(10, 0) NOT NULL PRIMARY KEY references object(id_object),
	guests_included number(10, 0) null,
	extra_people varchar2(20) null,
	minimum_nights number(10, 0) null,
	maximum_nights number(10, 0) null,
	has_availability NUMBER(1,0) null,
	availability_30 number(10, 0) null,
	availability_60 number(10, 0) null,
	availability_90 number(10, 0) null,
	availability_365 number(10, 0) null,
	property_type varchar2(100) null,
	room_type varchar2(100) null,
	accommodates number(10, 0) null,
	bathrooms number(10, 2) null,
	bedrooms number(10, 0) null,
	bed_type varchar2(100) null,
	amenities clob null,
	square_feet number(10, 0) null
);