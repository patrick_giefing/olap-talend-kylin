# Apache Kylin + Talend - Example #

[LaTeX-document (german)](./src/main/document/src/main.pdf)  
[relational version - because the specification was changed afterwards (german)](./src/main/document/relational.pdf)

## Talend process

![talend process](./src/main/document/src/screenshots/talend-process.png)

## Talend mappings

![talend mappings](./src/main/document/src/screenshots/talend-mappings.png)

## Kylin example query

![Kylin example query](./src/main/document/src/screenshots/kylin-05-query-02-grouped-query.png)
